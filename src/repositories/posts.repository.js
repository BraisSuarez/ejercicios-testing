import axios from 'axios';

export class PostsRepository {
  async getAllPosts() {
    return await (
      await axios.get('https://jsonplaceholder.typicode.com/posts')
    ).data;
  }

  async addPost(post) {
    const newPost = {
      title: post.title,
      body: post.content,
      userId: 1,
    };
    return await (
      await axios.post('https://jsonplaceholder.typicode.com/posts', newPost)
    ).data;
  }

  async updatePost(post) {
    const newPost = {
      id: post.id,
      title: post.title,
      body: post.content,
      userId: 1,
    };
    return await (
      await axios.put(
        `https://jsonplaceholder.typicode.com/posts/${post.id}`,
        newPost
      )
    ).data;
  }

  async deletePost(postId) {
    return await axios.delete(
      `https://jsonplaceholder.typicode.com/posts/${postId}`
    );
  }
}
