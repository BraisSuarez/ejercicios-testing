import { LitElement, html } from 'lit';

export class PostDetail extends LitElement {
  static get properties() {
    return {
      add: { type: Boolean },
      post: { type: Object },
      inputData: { type: Object },
      updatedPost: { type: Object },
    };
  }

  connectedCallback() {
    super.connectedCallback();

    this.add = true;

    const postList = document.querySelector('posts-list');
    postList.addEventListener('isolated-post', (e) => {
      this.post = e.detail.post[0];
      this.add = false;
    });
  }

  render() {
    return html`
      <h2>Post Detail</h2>
      <form id='detailForm'>

      <label for="title">Title</label>
      <input placeholder="Write your post title" type="text" id="title" name="title" .value=${
        this.post ? this.post.title : ''
      }></input>
      </br>
      <label for="body">Body</label>
      <textarea placeholder="Write here your post" type="text" id="body" name="body" .value=${
        this.post ? this.post.content : ''
      }></textarea>
      </br>
      <fieldset>
      ${
        !this.add
          ? html` <button class="generalButton" @click=${this.cancelHandler}>
                Cancel
              </button>
              <button class="generalButton" @click=${this.updatePost}>
                Update
              </button>
              <button class="cancelButton" @click=${this.deletePost}>
                Delete
              </button>`
          : html` <button class="generalButton" @click=${this.createPost}>
                Add
              </button>
              <button class="cancelButton" @click=${this.addHandler}>
                Cancel
              </button>`
      }
      </fieldset> 
      </form>
    `;
  }

  // Updates a specific post
  updatePost(e) {
    e.preventDefault();
    const title = document.getElementById('title').value;
    const content = document.getElementById('body').value;

    this.updatedPost = {
      id: this.post.id,
      title: title,
      content: content,
    };

    const postsList = document.querySelector('posts-list');
    postsList.updatePost(this.updatedPost);

    document.getElementById('detailForm').reset();
  }

  // Deletes a specific post
  deletePost(e) {
    e.preventDefault();

    const postsList = document.querySelector('posts-list');
    postsList.deletePost(this.post.id);

    document.getElementById('detailForm').reset();
  }

  // Creates a post
  createPost(e) {
    e.preventDefault();
    const title = document.getElementById('title').value;
    const content = document.getElementById('body').value;

    this.inputData = {
      title,
      content,
    };

    const postsList = document.querySelector('posts-list');
    postsList.createPost(this.inputData);

    document.getElementById('detailForm').reset();
  }

  addHandler(e) {
    e?.preventDefault();
    document.getElementById('detailForm').reset();
    this.add = true;
  }

  cancelHandler(e) {
    e.preventDefault();
    document.getElementById('detailForm').reset();
    this.add = true;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('post-detail', PostDetail);
