import { LitElement, html } from 'lit';
import { AllPostsUseCase } from '../usecases/all-posts.usecase';
import { AddPosts } from '../usecases/create-post.usecase';
import { DeletePost } from '../usecases/delete-post.usecase';
import { UpdatePost } from '../usecases/update-post.usecase';

export class PostsList extends LitElement {
  static get properties() {
    return {
      posts: { type: Array },
      morePosts: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();

    try {
      this.posts = await AllPostsUseCase.execute();
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return html`
      <h2>Posts List</h2>
      <button @click="${this.addHandler}">Add Post</button>
      <ul>
        ${this.posts?.map((post) => {
          return html` <li @click=${this.postHandler}>${post.title}</li> `;
        })}
      </ul>
    `;
  }

  async updatePost(post) {
    try {
      this.requestUpdate();
      const listUpdated = await UpdatePost.execute(post, this.posts);
      this.posts = listUpdated;
    } catch (error) {
      console.error(error);
    } finally {
      this.requestUpdate();
    }
  }

  async deletePost(id) {
    try {
      const filteredList = await DeletePost.execute(this.posts, id);
      this.posts = filteredList;
    } catch (error) {
      console.error(error);
    } finally {
      this.requestUpdate();
    }
  }

  async createPost(post) {
    try {
      const updatedPosts = await AddPosts.execute(post, this.posts);
      this.posts = updatedPosts;
    } catch (error) {
      console.error(error);
    } finally {
      this.requestUpdate();
    }
  }

  addHandler(e) {
    e.preventDefault();
    const detail = document.querySelector('post-detail');
    detail.addHandler();
  }

  postHandler(e) {
    e.preventDefault();

    const isolatedPost = this.posts.filter(
      (post) => post.title === e.target.innerText
    );

    this.dispatchEvent(
      new CustomEvent('isolated-post', {
        bubbles: true,
        composed: true,
        detail: { post: isolatedPost },
      })
    );
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('posts-list', PostsList);
