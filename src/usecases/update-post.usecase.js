import { PostsRepository } from '../repositories/posts.repository';
import { Post } from '../model/post';

export class UpdatePost {
  static async execute(post, POSTS) {
    const repository = new PostsRepository();
    const newPost = await repository.updatePost(post);

    const updatedPost = new Post({
      id: newPost.id,
      title: newPost.title,
      content: newPost.body,
    });

    return POSTS.map((post) =>
      post.id === updatedPost.id ? (post = updatedPost) : (post = post)
    );
  }
}
