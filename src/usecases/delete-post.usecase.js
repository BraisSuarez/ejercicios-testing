import { PostsRepository } from '../repositories/posts.repository';

export class DeletePost {
  static async execute(listOfPosts, postId) {
    const repository = new PostsRepository();
    await repository.deletePost(postId);

    return listOfPosts.filter((post) => post.id !== postId);
  }
}
