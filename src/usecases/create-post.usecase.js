import { Post } from '../model/post';
import { PostsRepository } from '../repositories/posts.repository';

export class AddPosts {
  static async execute(post, postList) {
    const repository = new PostsRepository();
    const sendPost = await repository.addPost(post);

    const newPost = new Post({
      id: sendPost.id,
      title: sendPost.title,
      content: sendPost.body,
    });

    return [...postList, newPost];
  }
}
