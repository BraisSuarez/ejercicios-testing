import { Post } from '../model/post';
import { PostsRepository } from '../repositories/posts.repository';

export class OddPosts {
  static async execute() {
    const repository = new PostsRepository();
    const posts = await repository.getAllPosts();
    return posts.filter((post) => {
      if (post.id % 2 === 1) {
        return new Post({ id: post.id, title: post.id, content: post.body });
      }
    });
  }
}
