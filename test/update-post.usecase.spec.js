import { Post } from '../src/model/post';
import { PostsRepository } from '../src/repositories/posts.repository';
import { UpdatePost } from '../src/usecases/update-post.usecase';
import { POSTS } from './fixtures/posts';

jest.mock('../src/repositories/posts.repository');

describe('Update post use case', () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });
  it('should executing properly', async () => {
    // Arrrange
    const post = new Post({
      id: 1,
      title: 'Updated Post',
      content: 'This is a new post',
    });

    PostsRepository.mockImplementation(() => {
      return {
        updatePost: () => {
          return {
            id: post.id,
            title: post.title,
            body: post.content,
            userId: 1,
          };
        },
      };
    });

    // Act
    const posts = await UpdatePost.execute(post, POSTS);

    // Assert
    expect(posts.length).toBe(100);
    expect(posts[0].title).toBe('Updated Post');
  });
});
