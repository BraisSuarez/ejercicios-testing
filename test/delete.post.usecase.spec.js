import { PostsRepository } from '../src/repositories/posts.repository';
import { DeletePost } from '../src/usecases/delete-post.usecase';

jest.mock('../src/repositories/posts.repository');

describe('delete post usecase', () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });
  it('should delete a post', async () => {
    // Arrange
    const postId = 2;

    const listOfPosts = [
      {
        id: 1,
        title: 'Post number 1',
        content: 'Post example 2',
      },
      {
        id: 2,
        title: 'Post number 2',
        content: 'Post example 2',
      },
      {
        id: 3,
        title: 'Post number 3',
        content: 'Post example 3',
      },
    ];

    PostsRepository.mockImplementation(() => {
      return {
        deletePost: () => {
          return listOfPosts;
        },
      };
    });

    // Act
    const listsWhitoutPost = await DeletePost.execute(listOfPosts, postId);

    // Assert
    expect(listsWhitoutPost.length).not.toBe(3);
    expect(listsWhitoutPost[1].id).toBe(3);
  });
});
