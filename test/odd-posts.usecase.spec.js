import { PostsRepository } from '../src/repositories/posts.repository';
import { POSTS } from './fixtures/posts';
import { OddPosts } from '../src/usecases/odd-posts.usecase';

jest.mock('../src/repositories/posts.repository');

describe('odd posts usecase', () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });
  it('should return odd posts', async () => {
    // Arrange
    const postList = [
      {
        userId: 1,
        id: 1,
        title: 'Title 1',
        content: 'Example 1',
      },
      {
        userId: 1,
        id: 2,
        title: 'Title 2',
        content: 'Example 2',
      },
      {
        userId: 1,
        id: 3,
        title: 'Title 3',
        content: 'Example 3',
      },
    ];

    PostsRepository.mockImplementation(() => {
      return {
        getAllPosts: () => {
          return postList;
        },
      };
    });

    // Act
    const posts = await OddPosts.execute();

    // Assert
    expect(posts[0].id % 2).toBe(1);
    expect(posts[1].id % 2).toBe(1);
  });
});
