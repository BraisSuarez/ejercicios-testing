import { Post } from '../src/model/post';
import { POSTS } from '../test/fixtures/posts';
import { AddPosts } from '../src/usecases/create-post.usecase';
import { PostsRepository } from '../src/repositories/posts.repository';

jest.mock('../src/repositories/posts.repository');

describe('add posts usecase', () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });
  it('should create a new post', async () => {
    // Arrange
    const post = new Post({ id: 1, title: 'Test', content: 'New post' });

    const postList = [
      {
        userId: 1,
        id: 1,
        title: 'Title 1',
        content: 'Example 1',
      },
      {
        userId: 1,
        id: 2,
        title: 'Title 2',
        content: 'Example 2',
      },
      {
        userId: 1,
        id: 3,
        title: 'Title 3',
        content: 'Example 3',
      },
    ];

    PostsRepository.mockImplementation(() => {
      return {
        addPost: () => {
          return {
            id: post.id,
            title: post.title,
            body: post.content,
            userId: 1,
          };
        },
      };
    });

    // Act
    const updatedPosts = await AddPosts.execute(post, postList);

    // Assert
    expect(updatedPosts.length).toBe(4);
    expect(updatedPosts[3].title).toBe('Test');
  });
});
