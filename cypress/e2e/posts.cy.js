describe('template spec', () => {
  it('passes', () => {
    cy.viewport(1536, 960);
    cy.visit('http://localhost:8080/');
    cy.get('ul > :nth-child(1)').click();
    cy.get('.cancelButton').click();
    cy.get('posts-list > button').click();
    cy.get('#title').type('Pagina de posts');
    cy.get('#body').type('Este es el textarea de la pagina de posts');
    cy.get('.generalButton').click();
    cy.get('ul > :nth-child(4)').click();
    cy.get('fieldset > :nth-child(1)').click();
  });
});
